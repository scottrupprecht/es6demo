const gulp = require("gulp");
const babel = require("gulp-babel");
const concat = require("gulp-concat");
const uglify = require('gulp-uglify');
const util = require('gulp-util');
const debug = require('gulp-debug');
const minifyCSS = require('gulp-minify-css');
const imagemin = require('gulp-imagemin');
const rename = require('gulp-rename');

var config = {
    production: !!util.env.production,
    defaultTasks: [
        "print-env", 
        "assets-js", 
        "js",
        "react-js",
        "css",
        "image-min",
        "move-fonts"
    ]
};

gulp.task("print-env", () => {
    util.log(`Running Gulp in ${config.production ? "Production" : "Development"} mode.`);
});

/*********************** Javascript ***********************/
gulp.task("assets-js", () => {
    return gulp.src([
            "Scripts/assets/jquery.js",
            "Scripts/assets/bootstrap.js",
            "Scripts/assets/prism.js",
            "Scripts/assets/underscore-min.js",
            config.production ? "Scripts/assets/react.min.js" : "Scripts/assets/react.js",
            config.production ? "Scripts/assets/react-dom.min.js" : "Scripts/assets/react-dom.js"
        ])
        .pipe(config.production ? uglify() : util.noop())
        .pipe(concat("assets.min.js"))    
        .pipe(gulp.dest("dist/js"))
});

gulp.task("js", () => {
    return gulp.src(["Scripts/**/*.js", "!Scripts/assets/*"])
        .pipe(babel({
            presets: ["es2015"]
        }))
        .pipe(config.production ? uglify() : util.noop()) 
        .pipe(concat("app.min.js"))
        .pipe(gulp.dest("dist/js"))
});

gulp.task("react-js", () => {
    return gulp.src(["Scripts/**/*.jsx", "!Scripts/assets/*"])
        .pipe(babel({
            presets: ["es2015", "react"]
        }))
        .pipe(config.production ? uglify() : util.noop()) 
        .pipe(rename({dirname: '', extname: ".min.js"}))
        .pipe(gulp.dest("dist/js/components"))
});

/*********************** CSS ***********************/
gulp.task("css", () => {
    return gulp.src([
            "Content/Styles/assets/bootstrap.css",
            "Content/Styles/assets/prism.css",
            "Content/Styles/Site.css"
        ])
    .pipe(config.production ? minifyCSS() : util.noop())
    .pipe(concat('style.min.css'))
    .pipe(gulp.dest('dist/css'))
});


gulp.task('image-min', () =>
    gulp.src('Content/Images/**/*.{png,gif,jpg,JPG,JPEG,JPG,PNG,GIF}')
        .pipe(imagemin())
        .pipe(rename({dirname: ""}))
        .pipe(gulp.dest('dist/img'))
);

gulp.task('move-fonts', () =>
    gulp.src('Content/fonts/*')
        .pipe(rename({dirname: ""}))
        .pipe(gulp.dest('dist/fonts'))
);


/*********************** Init ***********************/


gulp.task("default", config.defaultTasks, 
    () => {
    gulp.watch("Scripts/**/*",  ["js", "react-js"]);
    gulp.watch("Content/Styles/**/*",  ["css"]);
    gulp.watch("Content/Images/*",  ["image-min"]);
});