﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebLibrary.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TemplateStrings()
        {
            return View();
        }

        public ActionResult Classes()
        {
            return View();
        }

        public ActionResult React()
        {
            return View();
        }
    }
}