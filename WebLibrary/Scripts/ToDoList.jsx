﻿class TodoList extends React.Component {
    constructor() {
        super();
        this.id = 0;
        this.state = {
            todos: []
        };
    }
    render () {
        return (
            <div className="row">
                <div className="btn-group pull-right" role="group">
                    <button className="btn btn-default" onClick={this.printTodos.bind(this)}>Print Todos</button>
                    <button className="btn btn-success" onClick={this.addTodo.bind(this)}>Add Todo</button>
                </div>
                
                {_.size(this.state.todos) ? 
                <div className="col-md-4 col-md-offset-4">
                    <table className="table table-striped table-hover">
		                <thead>
			                <tr>
				                <th>Complete</th>
				                <th>Name</th>
			                </tr>
		                </thead>
		                <tbody>
		                    {
		                        _.map(this.state.todos, (todo) => {
		                            return (
						                <tr key={todo.id}>
							                <td><input type="checkbox" className="form-control" checked={todo.isChecked} onChange={this.setChecked.bind(this, todo.id)} /></td>
							                <td>{todo.name}</td>
						                </tr>
		                            );
		                        })
		                    }
		                </tbody>
                    </table>
                </div>
            : false}

            </div>
        );
    }
    addTodo () {
        const currentTodos = this.state.todos;
        const name = prompt("What is the todo?");
        const todo = {
            id: this.id++,
            name,
            isChecked: false
        }
        currentTodos.push(todo);
        this.setState({ todos: currentTodos });
    }
    setChecked(id) {
        const currentTodos = this.state.todos;
        _.each(currentTodos, (todo) => {
            if (todo.id !== id)
                return;
            todo.isChecked = !todo.isChecked;
        });

        this.setState({ todos: currentTodos });
    }

    printTodos() {
        console.log(this.state.todos);
    }
}